class profile::puppetmaster {

  class { 'r10k':
    remote => 'git@git.example.com:repos/puppet-control.git',
  }

  class { 'r10k::webhook::config':
    enable_ssl => false,
    protected  => false,
  }

  class { 'r10k::webhook':
    require => Class['r10k::webhook::config'],
  }

  include r10k::mcollective

  notify { 'master': }
}
